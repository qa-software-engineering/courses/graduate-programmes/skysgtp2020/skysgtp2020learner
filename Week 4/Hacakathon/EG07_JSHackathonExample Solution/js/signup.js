let titleValid = false;
let firstNameValid = false;
let lastNameValid = false;
let emailValid = false;
let submitButton = document.querySelector("button[type=submit]");

//Event handlers

document.querySelector("#form").addEventListener("submit", validateForm);
document.querySelector("#title").addEventListener("change", titleValidate);
document
  .querySelector("#firstName")
  .addEventListener("change", firstNameValidation);
document
  .querySelector("#lastName")
  .addEventListener("change", lastNameValidation);
document.querySelector("#email").addEventListener("change", emailValidation);

function validateForm(evt) {
  evt.preventDefault();
  if (firstNameValid && lastNameValid && emailValid) {
    alert("Data validated and submitted");
    return true;
  } else {
    alert("The form contains errors! Data not submitted.");
    return false;
  }
}

function checkSubmitEnabled() {
  if (titleValid && firstNameValid && lastNameValid && emailValid) {
    submitButton.disabled = false;
  }
}

function titleValidate() {
  const titleValue = document.querySelector("#title").value;
  titleValue ? (titleValid = true) : (titleValid = false);
  checkSubmitEnabled();
}

function firstNameValidation() {
  const firstNameRegex = /^[a-zA-Z]+$/;
  const firstName = document.forms[0]["firstName"].value;
  const firstNameInput = document.querySelector("#firstName");
  const msgFirstName = document.querySelector("#msgFirstName");
  if (firstName.length > 1 && firstName.length < 15) {
    if (!firstName.match(firstNameRegex)) {
      firstNameValid = false;
      displayMessage(
        msgFirstName,
        "Only lower and upper case letters are allowed"
      );
      submitButton.disabled = true;
    } else {
      firstNameValid = true;
      removeMessage(msgFirstName);
      checkSubmitEnabled();
    }
  } else {
    firstNameValid = false;
    displayMessage(
      msgFirstName,
      "The First Name entered is either too short or too long."
    );
    submitButton.disabled = true;
  }
  displayErrorBorder(firstNameValid, firstNameInput);
}

function lastNameValidation() {
  const lastNameRegex = /^[a-zA-Z\'\-]+$/g;
  const lastName = document.forms[0]["lastName"].value;
  const lastNameInput = document.querySelector("#lastName");
  const msgLastName = document.querySelector("#msgLastName");
  if (lastName.length > 1 && lastName.length < 25) {
    if (!lastName.match(lastNameRegex)) {
      lastNameValid = false;
      displayMessage(
        msgLastName,
        "Only letters, hyphens and apostrophes are allowed."
      );
      submitButton.disabled = true;
    } else {
      lastNameValid = true;
      removeMessage(msgLastName);
      checkSubmitEnabled();
    }
  } else {
    lastNameValid = false;
    displayMessage(
      msgLastName,
      "The Last Name entered is either too short or too long."
    );
  }
  displayErrorBorder(lastNameValid, lastNameInput);
}

function emailValidation() {
  const emailRegex = /[^\s@]+@[^\s@]+\.[^\s@]+/;
  const email = document.forms[0]["email"].value;
  const emailInput = document.querySelector("#email");
  const msgEmail = document.querySelector("#msgEmail");
  if (!email.match(emailRegex)) {
    emailValid = false;
    displayMessage(msgEmail, "Email address not valid");
    submitButton.disabled = true;
  } else {
    emailValid = true;
    removeMessage(msgEmail);
    checkSubmitEnabled();
  }
  displayErrorBorder(emailValid, emailInput);
}

function displayErrorBorder(inputValid, element) {
  if (!inputValid) {
    if (!element.classList.contains("invalid")) {
      element.classList.toggle("invalid");
    }
  } else {
    if (element.classList.contains("invalid")) {
      element.classList.toggle("invalid");
    }
  }
}

function displayMessage(element, message) {
  removeMessage(element);
  const msgcontent = document.createElement("div");
  msgcontent.id = "msgcontent";
  element.appendChild(msgcontent);
  msgcontent.innerHTML = message;
}

function removeMessage(element) {
  if (element.childNodes.length > 0) {
    const noChildren = element.childNodes.length;
    for (i = 0; i < noChildren; i++) {
      element.removeChild(element.firstChild);
    }
  }
}
